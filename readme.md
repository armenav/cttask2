# CT task management

### Getting Started

- create your DB
- update settings in .env file
- run php artisan migrate
- run php artisan db:seed --class=ProjectsTableSeeder
- enjoy !

### Things that can be improved

- Write tests
- Refactor validation duplications to specialized classes
- Add foreign keys on the database level into migrations

  
