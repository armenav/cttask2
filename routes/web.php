<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get( '/', function () {

	$project = \App\Project::all()->first();

	return view( 'welcome', compact( 'project' ) );

} );

// View tasks by project
Route::get( '/projects/{project}/tasks', 'TasksController@index' );

// View create new task form
Route::get( '/tasks/create', 'TasksController@create' );

// Save a new task
Route::post( '/tasks', 'TasksController@store' );

// View existing task
Route::get( '/tasks/{task}', 'TasksController@show' );

// View edit task form
Route::get( '/tasks/{task}/edit', 'TasksController@edit' );

// Save task being edited
Route::patch( '/tasks/{task}', 'TasksController@update' );

// Delete task
Route::delete( '/tasks/{task}', 'TasksController@destroy' );

// Prioritize tasks
Route::post( '/tasks/prioritize', 'TasksController@prioritize' );
