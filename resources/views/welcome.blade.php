@extends('layouts.master')


@section ('content')
    <div class="jumbotron text-center">
        <div class="container">
            <h1>Welcome to task management system</h1>
            <p>To get started go to tasks</p>
            <p>
                <a href="projects/{{$project->id}}/tasks" class="btn btn-primary btn-lg">Show tasks</a>
            </p>
        </div>
    </div>

@endsection

