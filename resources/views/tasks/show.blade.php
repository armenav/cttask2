@extends('layouts.master')


@section ('content')
    <div class="row">
        <a class="btn btn-default" href="/projects/{{$task->project->id}}/tasks">Go Back</a>
        <a class="btn btn-default" href="/tasks/{{$task->id}}/edit">Edit task</a>
        <form method="post" action="" style="display: inline-block;">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-danger">Delete task</button>
        </form>

    </div>
    <div class="row">

    </div>

    <h1>{{$task->name}}</h1>
@endsection

