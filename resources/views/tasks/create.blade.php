@extends('layouts.master')


@section ('content')
    <div class="row">
        <a href="/projects/{{$projects[0]->id}}/tasks">View tasks by projects</a>
    </div>
    <hr>
    <form method="post" action="/tasks">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Task Name</label>
            <input type="text" class="form-control" id="name" placeholder="" name="name" required>
        </div>

        <div class="form-group">
            <label for="project" class="control-label">Project</label>
            <select class="form-control" id="project" name="project">
                @foreach($projects as $project)
                    <option value="{{$project->id}}">{{$project->name}}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>

    </form>

@endsection

