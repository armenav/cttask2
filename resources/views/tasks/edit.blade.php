@extends('layouts.master')


@section ('content')
    <div class="row">
        <a href="/projects/{{$projects[0]->id}}/tasks">View tasks by projects</a>
    </div>
    <hr>
    <form method="post" action="{{url('/tasks')}}/{{$task->id}}">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        <div class="form-group">
            <label for="name">Task Name</label>
            <input value="{{$task->name}}" type="text" class="form-control" id="name" name="name" required>
        </div>
        <div class="form-group">
            <label for="project" class="control-label">Project</label>
            <select class="form-control" id="project" name="project">
                @foreach($projects as $project)
                    <option value="{{$project->id}}"
                            @if($task->project->id == $project->id)
                            selected
                            @endif
                    >{{$project->name}}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>

    </form>

@endsection

