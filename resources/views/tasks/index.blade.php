@extends('layouts.master')



@section ('content')
    <div class="row">
        <a href="/tasks/create">Create New Task</a>
    </div>
    <hr>
    <style>
        #sortable {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        #sortable li {
            margin: 5px 5px 3px 3px;
            padding: 0.4em;
            font-size: 1.4em;
            background: #DDDDDD;
            height: 40px;
        }
    </style>


    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="form-group">
                <label for="project" class="control-label">Project</label>
                <select class="form-control" name="project" id="project" onchange="window.location = $(this).val();">
                    @foreach($projects as $project)
                        <option value="{{URL::to('/')}}/projects/{{$project->id}}/tasks"
                                @if($selectedProject->id == $project->id)
                                selected
                                @endif
                        >{{$project->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-md-4 col-md-offset-4">
            <ul id="sortable">
                @foreach($tasks as $task)
                    <li id="task-{{$task->id}}" class="ui-state-default"><a
                                href="/tasks/{{$task->id}}">{{$task->name}}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection

