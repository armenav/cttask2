(function ($) {

    $(document).ready(function () {

        $(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#sortable').sortable({
                axis: 'y',
                update: function (event, ui) {

                    var data = $(this).sortable('serialize');

                    console.log(data);
                    $.ajax({
                        data: data,
                        dataType: 'json',
                        type: 'POST',
                        url: '/tasks/prioritize',
                        success: function (data) {
                            console.log(data);
                        },
                        error: function (data) {
                            var errors = data.responseJSON;

                            console.log(errors);

                            alert('Could not change the order of tasks');
                            window.location.reload();

                        }
                    });
                }
            });

            $("#sortable").disableSelection();
        });


    });

})(jQuery);
