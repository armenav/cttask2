<?php

use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('projects')->insert([
		    ['name' => 'project 1'],
		    ['name' => 'project 2'],
		    ['name' => 'project 3'],
		    ['name' => 'project 4'],
	    ]);
    }
}
