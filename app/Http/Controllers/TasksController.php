<?php

namespace App\Http\Controllers;

use App\Project;
use App\Task;
use Illuminate\Http\Request;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project)
    {

    	$projects = Project::all();

	    $tasks = $project->tasks()->orderBy('priority')->get();

	    return view( 'tasks.index' )
		    ->with( compact( [ 'tasks', 'projects', 'selectedProject' ] ) )
		    ->with( [ 'selectedProject' => $project ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$projects = Project::all();

		return view('tasks.create', compact('projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
	    $this->validate( \request(), [
			    'name'    => 'required',
			    'project' => 'required',
		    ]
	    );

	    $task = new Task();

	    $task->name = request( 'name' );

	    $task->priority = Task::all()->count() + 1;

	    $task->project_id = request( 'project' );

	    $task->save();

    	return redirect("/projects/{$task->project_id}/tasks")->withSuccess('Task created successfully!');;
    }

    /**
     * Display the specified resource.
     *
     * @param  Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
	    return view('tasks.show', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
	    $projects = Project::all();

	    return view('tasks.edit', compact(['task', 'projects']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Task $task)
    {

	    $this->validate(\request(), [
			    'name'=>'required',
			    'project' => 'required',
		    ]
	    );

	    $task->name = request( 'name' );

	    $task->project_id = request( 'project' );

	    $task->save();

	    return redirect("/projects/{$task->project_id}/tasks");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {

	    $task->delete();

	    return redirect("projects/{$task->project->id}/tasks");
    }

    /**
     * Prioritize tasks
     *
     * @return \Illuminate\Http\Response
     */
    public function prioritize()
    {

	    $taskIdsPrioritized = request('task');
	    $project = Task::whereId( $taskIdsPrioritized[0] )->first()->project;

	    $i = 0;
	    foreach ( $taskIdsPrioritized as $id ) {
		    $task           = Task::whereId( $id )->first();
		    $task->priority = ++$i;
		    $task->save();
	    }
    	return compact(['project', 'taskIdsPrioritized']);

    }
}
